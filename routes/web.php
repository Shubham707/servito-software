<?php

use Illuminate\Support\Facades\Auth;
use App\Http\Middleware\Authenticate;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\ThemeController;
use App\Http\Controllers\SystemController;
use App\Http\Controllers\LandingController;
use App\Http\Controllers\SubmenuController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     // $landings = session('landings');
//     return view('welcome');
// });



Route::get('pages/{first}/{second}', [PagesController::class, 'pages']);
Route::get('/', [PagesController::class, 'pages']);

Auth::routes();

Route::controller(HomeController::class)->group(function () {
    Route::get('/dashboard', 'index')->name('dashboard');
});


Route::group(['prefix' => 'admin', ['middleware' => 'auth']], function () {
    Route::get('/landing', [LandingController::class, 'index'])->name('landing');
    Route::get('/theme-connections', [ThemeController::class, 'connections'])->name('theme-connections');
    Route::get('/theme-setup', [ThemeController::class, 'themes'])->name('theme-setup');
    Route::get('/theme-settings', [ThemeController::class, 'settings'])->name('theme-settings');
    Route::post('/landing-add', [LandingController::class, 'store'])->name('landing-add');
    Route::get('/system', [SystemController::class, 'index'])->name('system');
    // logos setup
    Route::post('/add-logo', [SystemController::class, 'store'])->name('admin.add-logo');
    Route::get('/admin.setting-edit', [SystemController::class, 'edit'])->name('admin.setting-edit');
    Route::get('/setting-status', [SystemController::class, 'statusChange'])->name('admin.setting-status');
    //CLose
    route::resources(['menus' => MenuController::class]);
    Route::get('fetch', [MenuController::class, 'fetchMenu'])->name('menus.fetch');

    route::resources(['submenus' => SubmenuController::class]);
    Route::get('fetch-sub-menu', [SubmenuController::class, 'fetchSubMenu'])->name('submenus.fetch');

    route::resources(['themes' => ThemeController::class]);
});
