<div class="menu-inner-shadow"></div>

<ul class="menu-inner py-1">
  <!-- Dashboard -->
  <li class="menu-item active">
    <a href="{{route('dashboard')}}" class="menu-link">
      <i class="menu-icon tf-icons bx bx-home-circle"></i>
      <div data-i18n="Analytics">Dashboard</div>
    </a>
  </li>
  
  <li class="menu-item">
    <a href="javascript:void(0);" class="menu-link menu-toggle">
      <i class="menu-icon tf-icons bx bx-dock-top"></i>
      
      <div data-i18n="Account Settings">Manu & Submenu</div>
    </a>
    <ul class="menu-sub">
    <li class="menu-item">
        <a href="{{route('menus.index')}}" class="menu-link">
          <div data-i18n="Notifications">Menu</div>
        </a>
      </li>
      <li class="menu-item">
        <a href="{{route('submenus.index')}}" class="menu-link">
          <div data-i18n="Account">SubMenu</div>
        </a>
      </li>
    </ul>
  </li>
  <li class="menu-item">
    <a href="javascript:void(0);" class="menu-link menu-toggle">
      <i class='menu-icon tf-icons bx bxs-book-open'></i>
      <div data-i18n="Account Settings">Landing Pages</div>
    </a>
    <?php
      // use \App\Models\Menu;
      // $menuList=Menu::all();
    ?>
    <ul class="menu-sub">
      <!-- foreach($menuList as $list) strtolower($list->menu_name))}}-->
      <li class="menu-item">
        <a href="{{route('landing')}}" class="menu-link">
          <div data-i18n="Notifications">Landing Page</div>
        </a>
      </li>
    <!-- endforeach -->
    </ul>
  </li>
  <li class="menu-item">
    <a href="javascript:void(0);" class="menu-link menu-toggle">
      <i class='menu-icon tf-icons bx bxs-book-reader'></i>
      <div data-i18n="Account Settings">Theme</div>
    </a>
    <ul class="menu-sub">
      <li class="menu-item">
        <a href="{{route('themes.index')}}" class="menu-link">
          <div data-i18n="Notifications">Setup</div>
        </a>
      </li>
      <li class="menu-item">
        <a href="{{route('theme-connections')}}" class="menu-link">
          <div data-i18n="Notifications">Theme Connection</div>
        </a>
      </li>
      <li class="menu-item">
        <a href="{{route('theme-settings')}}" class="menu-link">
          <div data-i18n="Notifications">Theme Settings</div>
        </a>
      </li>
    </ul>
  </li>
</ul>
</aside>
<!-- / Menu -->

<!-- Layout container -->
<div class="layout-page">
<!-- Navbar -->

<nav
  class="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme"
  id="layout-navbar">
  <div class="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
    <a class="nav-item nav-link px-0 me-xl-4" href="javascript:void(0)">
      <i class="bx bx-menu bx-sm"></i>
    </a>
  </div>

  <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
    <!-- Search -->
    <div class="navbar-nav align-items-center">
      <div class="nav-item d-flex align-items-center">
        <i class="bx bx-search fs-4 lh-0"></i>
        <input
          type="text"
          class="form-control border-0 shadow-none ps-1 ps-sm-2"
          placeholder="Searchadmin."
          aria-label="Searchadmin." />
      </div>
    </div>
    <!-- /Search -->

    <ul class="navbar-nav flex-row align-items-center ms-auto">
      <!-- Place this tag where you want the button to render. -->
      <li class="nav-item lh-1 me-3">
        <a
          class="github-button"
          href="https://github.com/themeselection/sneat-html-admin-template-free"
          data-icon="octicon-star"
          data-size="large"
          data-show-count="true"
          aria-label="Star themeselection/sneat-html-admin-template-free on GitHub"
          >Star</a
        >
      </li>

      <!-- User -->
      <li class="nav-item navbar-dropdown dropdown-user dropdown">
        <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown">
          <div class="avatar avatar-online">
            <img src="{{asset('admin/assets/img/avatars/1.png')}}" alt class="w-px-40 h-auto rounded-circle" />
          </div>
        </a>
        <ul class="dropdown-menu dropdown-menu-end">
          <li>
            <a class="dropdown-item" href="#">
              <div class="d-flex">
                <div class="flex-shrink-0 me-3">
                  <div class="avatar avatar-online">
                    <img src="admin/assets/img/avatars/1.png" alt class="w-px-40 h-auto rounded-circle" />
                  </div>
                </div>
                <div class="flex-grow-1">
                  <span class="fw-medium d-block">John Doe</span>
                  <small class="text-muted">Admin</small>
                </div>
              </div>
            </a>
          </li>
          <li>
            <div class="dropdown-divider"></div>
          </li>
          <li>
            <a class="dropdown-item" href="#">
              <i class="bx bx-user me-2"></i>
              <span class="align-middle">My Profile</span>
            </a>
          </li>
          <li>
            <a class="dropdown-item" href="{{route('system')}}">
              <i class="bx bx-cog me-2"></i>
              <span class="align-middle">Settings</span>
            </a>
          </li>
          <li>
            <a class="dropdown-item" href="#">
              <span class="d-flex align-items-center align-middle">
                <i class="flex-shrink-0 bx bx-credit-card me-2"></i>
                <span class="flex-grow-1 align-middle ms-1">Billing</span>
                <span class="flex-shrink-0 badge badge-center rounded-pill bg-danger w-px-20 h-px-20">4</span>
              </span>
            </a>
          </li>
          <li>
            <div class="dropdown-divider"></div>
          </li>
          <li>
            <a class="dropdown-item" href="javascript:void(0);">
             
              <form method="POST" action="{{ route('logout') }}">
                  @csrf
                  <button class="btn btn-info" type="submit">  <i class="bx bx-power-off me-2"></i> Log Out</button>
                </form>
             
            </a>
          </li>
        </ul>
      </li>
      <!--/ User -->
    </ul>
  </div>
</nav>

<!-- / Navbar -->