<!-- Footer -->
<?php
use App\Models\System;

$logos = System::where('status', '1')
    ->orWhere('status', '0')
    ->first();
?>
<footer class="content-footer footer bg-footer-theme">
    <div class="container-xxl d-flex flex-wrap justify-content-between py-2 flex-md-row flex-column">
        <div class="mb-2 mb-md-0">
            ©
            <script>
                document.write(new Date().getFullYear());
            </script>
            design by
            <a href="javascript:void(0)" target="_blank" class="footer-link fw-medium" style="color:blue;">
                @if ($logos->title !== null)
                    {{ $logos->title }}
                @else
                    {{ 'Servito Software' }}
                @endIf
            </a>
        </div>
        <div class="d-none d-lg-inline-block">


            <a href="https://github.com/themeselection/sneat-html-admin-template-free/issues" target="_blank"
                class="footer-link me-4">Support</a>
        </div>
    </div>
</footer>
<!-- / Footer -->

<div class="content-backdrop fade"></div>
</div>
<!-- Content wrapper -->
</div>
<!-- / Layout page -->
</div>

<!-- Overlay -->
<div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->

<!-- Core JS -->
<!-- build:js assets/vendor/js/core.js')}} -->

<script src="{{ asset('admin/assets/vendor/libs/jquery/jquery.js') }}"></script>
<script src="{{ asset('admin/assets/vendor/libs/popper/popper.js') }}"></script>
<script src="{{ asset('admin/assets/vendor/js/bootstrap.js') }}"></script>
<script src="{{ asset('admin/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
<script src="{{ asset('admin/assets/vendor/js/menu.js') }}"></script>

<!-- endbuild -->
<script src="{{ asset('admin/assets/js/main.js') }}"></script>

<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Helpers -->
<script src="{{ asset('admin/assets/vendor/js/helpers.js') }}"></script>
<!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
<!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
<script src="{{ asset('admin/assets/js/config.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"
    integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>

@yield('javascript')
<!-- Vendors JS -->

<!-- <script src="{{ asset('admin/assets/js/ui-modals.js') }}"></script> -->
<!-- Place this tag in your head or just before your close body tag. -->

</body>

</html>
