<?php
use App\Models\Theme;
$files = Theme::all();
// $files=Theme::where('theme_status','Active')->orWhere('theme_status','0')->first();
?>

@include('layouts.theme.header')
@include('layouts.theme.sidebar')

<div class="content-wrapper">
    @yield('content')
</div>
@include('layouts.theme.footer')
@stack('menu_scripts')
