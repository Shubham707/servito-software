@extends('layouts.master')
@section('css')

@stop
@section('content')
    <style>
        .toast {
            background-color: #333;
            color: #fff;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
        }

        .toast-success {
            background-color: #2ecc71;
        }

        .toast-info {
            background-color: #3498db;
        }

        .toast-warning {
            background-color: #f39c12;
        }

        .toast-error {
            background-color: #e74c3c;
        }
    </style>
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <div class="col-md-12 col-lg-12 order-2 mb-4">
                <div class="card">
                    <h5 class="card-header">Menu List </h5>
                    <div class="card-body">
                        <button type="button" class="btn btn-primary float-md-right" id="addMenu">Add Menu</button>
                        <div class="table-responsive text-nowrap">
                            <table class="table" id="menuTable">
                                <thead>
                                    <tr>
                                        <th>Menu Name</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="table-border-bottom-0"></tbody>
                            </table>
                        </div>
                    </div>
                </div>

                {{-- Add Menu Modal --}}
                <div class="modal fade" id="addModal" data-bs-backdrop="static" tabindex="-1">
                    <div class="modal-dialog">
                        <form class="modal-content" id="addMenuForm" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-header">
                                <h5 class="modal-title" id="backDropModalTitle">Menu name</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col mb-3">
                                        <label for="menu_name" class="form-label">Name</label>
                                        <input type="text" id="menu_name" name="menu_name" class="form-control"
                                            placeholder="Enter Menu" />
                                        <span class="text-danger" id="menu_error"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">
                                    Close
                                </button>
                                <button type="submit" class="btn btn-primary btn-sm">Save</button>
                            </div>
                        </form>
                    </div>
                </div>

                {{-- Edit Menu Modal --}}
                <div class="modal fade" id="editModal" data-bs-backdrop="static" tabindex="-1">
                    <div class="modal-dialog">
                        <form class="modal-content" id="editMenuForm" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-header">
                                <h5 class="modal-title" id="backDropModalTitle">Menu name</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col mb-3">
                                        <label for="edit_menu_name" class="form-label">Name</label>
                                        <input type="text" id="edit_menu_name" name="edit_menu_name" class="form-control"
                                            placeholder="Enter Menu" />
                                        <input type="hidden" name="edit_menu_id" id="edit_menu_id">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">
                                    Close
                                </button>
                                <button type="submit" id="update_menu_btn" class="btn btn-primary btn-sm">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('menu_scripts')
    <script>
        // Fetch Menu
        fetchMenu();

        function fetchMenu() {
            $.ajax({
                url: "{{ route('menus.fetch') }}",
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    $('#menuTable tbody').empty();
                    $.each(data, function(index, menu) {
                        var row = '<tr>' +
                            '<td>' +
                            '<i class="fab fa-angular fa-lg text-danger me-3"></i>' +
                            '<span class="fw-medium">' + menu.menu_name + '</span>' +
                            '</td>' +
                            '<td>' +
                            '<div class="dropdown">' +
                            '<button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">' +
                            '<i class="bx bx-dots-vertical-rounded"></i>' +
                            '</button>' +
                            '<div class="dropdown-menu">' +
                            '<a class="dropdown-item menu-edit" data-id="' + menu.id +
                            '" href="javascript:void(0);"><i class="bx bx-edit-alt me-1"></i> Edit</a>' +
                            '<a class="dropdown-item menu-delete" data-id="' + menu.id +
                            '" href="javascript:void(0);"><i class="bx bx-trash me-1"></i> Delete</a>' +
                            '</div>' +
                            '</div>' +
                            '</td>' +
                            '</tr>';
                        $('#menuTable tbody').append(row);
                    });
                },
                error: function() {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        positionClass: "toast-top-right",
                        timeOut: 5000,
                    }
                    toastr.error('Error Something went wrong.');
                }
            });
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function() {
            // Add Menu
            $('#addMenu').click(function() {
                $('#addModal').modal('show');
            });

            $('#addMenuForm').on('submit', function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                $('#addMenuForm input').on('input', function() {
                    var fieldName = $(this).attr('name');
                    clearFieldError(fieldName);
                });
                $.ajax({
                    type: "POST",
                    url: "{{ route('menus.store') }}",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.status == true) {
                            $('#addModal').modal('hide');
                            $('#addMenuForm')[0].reset();
                            toastr.options = {
                                closeButton: true,
                                progressBar: true,
                                positionClass: "toast-top-right",
                                timeOut: 5000,
                            }
                            toastr.success(response.message);
                            fetchMenu();
                        } else {
                            for (var field in response.errors) {
                                if (response.errors.hasOwnProperty(field)) {
                                    toastr.options = {
                                        closeButton: true,
                                        progressBar: true,
                                        positionClass: "toast-top-right",
                                        timeOut: 5000,
                                    }
                                    toastr.error(response.errors[field][0]);
                                }
                            }
                        }
                    }
                });
            });

            // Fetch Edit Menu
            $(document).on("click", ".menu-edit", function() {
                $("#editModal").modal("show");
                var menu_id = $(this).data("id");
                var editUrl = "{{ url('admin/menus') }}/" + menu_id + "/edit";
                $.ajax({
                    url: editUrl,
                    type: "get",
                    dataType: "json",
                    success: function(response) {
                        if (response.status == true) {
                            var result = response.menu;
                            $("#edit_menu_name").val(result.menu_name);
                            $("#edit_menu_id").val(result.id);
                        } else {
                            console.log("Something went wrong.");
                        }
                    },
                    error: function() {
                        toastr.options = {
                                closeButton: true,
                                progressBar: true,
                                positionClass: "toast-top-right",
                                timeOut: 5000,
                            },
                            toastr.error('Error Something went wrong.');
                    }
                });
            });

            // Submit edit Main Category
            $("#update_menu_btn").click(function(e) {
                e.preventDefault();
                var edit_menu_id = $('#edit_menu_id').val();
                var edit_menu_name = $('#edit_menu_name').val();
                var updateUrl = "{{ url('admin/menus') }}/" + edit_menu_id;

                $.ajax({
                    url: updateUrl,
                    type: "PATCH",
                    data: {
                        edit_menu_name: edit_menu_name,
                    },
                    success: function(response) {
                        if (response.status == true) {
                            $('#editModal').modal('hide');
                            $('#edit_menu_name').val('');
                            toastr.options = {
                                closeButton: true,
                                progressBar: true,
                                positionClass: "toast-top-right",
                                timeOut: 5000,
                            }
                            toastr.success(response.message);
                            fetchMenu();
                        } else {
                            for (var field in response.errors) {
                                if (response.errors.hasOwnProperty(field)) {
                                    toastr.options = {
                                        closeButton: true,
                                        progressBar: true,
                                        positionClass: "toast-top-right",
                                        timeOut: 5000,
                                    }
                                    toastr.error(response.errors[field][0]);
                                }
                            }
                        }
                    }
                });
            });

            $(document).on("click", ".menu-delete", function() {
                var delete_menu_id = $(this).data("id");
                var deleteUrl = "{{ url('admin/menus') }}/" + delete_menu_id;
                if (confirm("Are you sure you want to delete this Menu?")) {
                    $.ajax({
                        type: "DELETE",
                        url: deleteUrl,
                        success: function(response) {
                            if (response.status) {
                                toastr.options = {
                                    closeButton: true,
                                    progressBar: true,
                                };
                                toastr.success(response.message);
                                fetchMenu();
                            } else {
                                toastr.options = {
                                    closeButton: true,
                                    progressBar: true,
                                };
                                toastr.error(response.message);
                            }
                        },
                        error: function(error) {
                            toastr.error(error);
                        },
                    });
                }
            });
        });
    </script>
@endpush
