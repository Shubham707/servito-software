@extends('layouts.master')
@section('css')

@stop
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <div class="col-md-12 col-lg-12 order-2 mb-4">
                <div class="card">
                    <h5 class="card-header">Submenu List </h5>
                    <div class="card-body">
                        <button type="button" class="btn btn-primary float-md-right" id="addSubmenu">Add Submenu</button>
                        <div class="table-responsive text-nowrap">
                            <table class="table" id="subMenuTable">
                                <thead>
                                    <tr>
                                        <th>Menu </th>
                                        <th>Submenu</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="table-border-bottom-0"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {{-- Add Submenu Modal --}}
                <div class="modal fade" id="addSubmenuModal" data-bs-backdrop="static" tabindex="-1">
                    <div class="modal-dialog">
                        <form class="modal-content" id="addSubmenuForm" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-header">
                                <h5 class="modal-title">Modal title</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="mt-2 mb-3">
                                        <label for="menu" class="form-label">Menu Select</label>
                                        <select id="menu" name="menu" class="form-select">
                                            <option value="" selected disabled>Select Menu </option>
                                            @foreach ($menus as $menu)
                                                <option value="{{ $menu->id }}">{{ $menu->menu_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col mb-3">
                                        <label for="submenu" class="form-label">Submenu</label>
                                        <input type="text" id="submenu" name="submenu" class="form-control"
                                            placeholder="Enter Submenu" />
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">
                                    Close
                                </button>
                                <button type="submit" class="btn btn-primary btn-sm">Save</button>
                            </div>
                        </form>
                    </div>
                </div>

                {{-- Edit Submenu Modal --}}
                <div class="modal fade" id="editSubmenuModal" data-bs-backdrop="static" tabindex="-1">
                    <div class="modal-dialog">
                        <form class="modal-content" id="editSubmenuForm" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-header">
                                <h5 class="modal-title">Modal title</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="mt-2 mb-3">
                                        <input type="hidden" name="edit_menu_id" id="edit_menu_id">
                                        <label for="edit_menu" class="form-label">Menu Select</label>
                                        <select id="edit_menu" name="edit_menu" class="form-select form-select-lg">
                                            <option value="" selected disabled>Select Menu </option>
                                            @foreach ($menus as $menu)
                                                <option value="{{ $menu->id }}">{{ $menu->menu_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col mb-3">
                                        <label for="edit_submenu" class="form-label">Submenu</label>
                                        <input type="text" id="edit_submenu" name="edit_submenu" class="form-control"
                                            placeholder="Enter Submenu" />
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">
                                    Close
                                </button>
                                <button type="submit" id="update_submenu_btn" class="btn btn-primary btn-sm">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('menu_scripts')
    <script>
        // Fetch Menu
        fetchSubMenu();

        function fetchSubMenu() {
            $.ajax({
                url: "{{ route('submenus.fetch') }}",
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    $('#subMenuTable tbody').empty();
                    if (response.status && response.submenu.length > 0) {
                        $.each(response.submenu, function(index, submenu) {
                            var row = '<tr>' +
                                '<td>' + submenu.menu_name + '</td>' +
                                '<td>' + submenu.submenu_name + '</td>' +
                                '<td>' +
                                '<div class="dropdown">' +
                                '<button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">' +
                                '<i class="bx bx-dots-vertical-rounded"></i>' +
                                '</button>' +
                                '<div class="dropdown-menu">' +
                                '<a class="dropdown-item menu-edit" data-id="' + submenu.id +
                                '" href="javascript:void(0)"><i class="bx bx-edit-alt me-1"></i> Edit</a>' +
                                '<a class="dropdown-item menu-delete" data-id="' + submenu.id +
                                '" href="javascript:void(0)"><i class="bx bx-trash me-1"></i> Delete</a>' +
                                '</div>' +
                                '</div>' +
                                '</td>' +
                                '</tr>';
                            $('#subMenuTable tbody').append(row);
                        });
                    } else {
                        $('#subMenuTable tbody').html('<tr><td colspan="3">No submenu found.</td></tr>');
                    }
                },
                error: function() {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        positionClass: "toast-top-right",
                        timeOut: 5000,
                    }
                    toastr.error('Error Something went wrong.');
                }
            });
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function() {
            // Add Submenu
            $('#addSubmenu').click(function() {
                $('#addSubmenuModal').modal('show');
            });
            $('#addSubmenuForm').on('submit', function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('submenus.store') }}",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.status == true) {
                            $('#addSubmenuModal').modal('hide');
                            $('#addSubmenuForm')[0].reset();
                            toastr.options = {
                                closeButton: true,
                                progressBar: true,
                                positionClass: "toast-top-right",
                                timeOut: 5000,
                            }
                            toastr.success(response.message);
                            fetchSubMenu();
                        } else {
                            for (var field in response.errors) {
                                if (response.errors.hasOwnProperty(field)) {
                                    toastr.options = {
                                        closeButton: true,
                                        progressBar: true,
                                        positionClass: "toast-top-right",
                                        timeOut: 5000,
                                    }
                                    toastr.error(response.errors[field][0]);
                                }
                            }
                        }
                    }
                });
            });

            // Fetch Edit Menu
            $(document).on("click", ".menu-edit", function() {
                $("#editSubmenuModal").modal("show");
                var menu_id = $(this).data("id");
                var editUrl = "{{ url('admin/submenus') }}/" + menu_id + "/edit";
                $.ajax({
                    url: editUrl,
                    type: "get",
                    dataType: "json",
                    success: function(response) {
                        if (response.status == true) {
                            var result = response.submenu;
                            $("#edit_menu_id").val(result.id);
                            $("#edit_menu").val(result.id);
                            $("#edit_submenu").val(result.submenu_name);
                        } else {
                            console.log("Something went wrong.");
                        }
                    },
                    error: function() {
                        toastr.options = {
                                closeButton: true,
                                progressBar: true,
                                positionClass: "toast-top-right",
                                timeOut: 5000,
                            },
                            toastr.error('Error Something went wrong.');
                    }
                });
            });

            // Submit edit Main Category
            $("#update_submenu_btn").click(function(e) {
                e.preventDefault();
                var edit_menu_id = $('#edit_menu_id').val();
                var menu = $('#edit_menu').val();
                var submenu = $('#edit_submenu').val();
                var updateUrl = "{{ url('admin/submenus') }}/" + edit_menu_id;

                $.ajax({
                    url: updateUrl,
                    type: "PATCH",
                    data: { menu: menu, submenu: submenu },
                    success: function(response) {
                        if (response.status == true) {
                            $('#editSubmenuModal').modal('hide');
                            $('#edit_menu').val('');
                            $('#edit_submenu').val('');
                            toastr.options = {
                                closeButton: true,
                                progressBar: true,
                                positionClass: "toast-top-right",
                                timeOut: 5000,
                            }
                            toastr.success(response.message);
                            fetchSubMenu();
                        } else {
                            for (var field in response.errors) {
                                if (response.errors.hasOwnProperty(field)) {
                                    toastr.options = {
                                        closeButton: true,
                                        progressBar: true,
                                        positionClass: "toast-top-right",
                                        timeOut: 5000,
                                    }
                                    toastr.error(response.errors[field][0]);
                                }
                            }
                        }
                    }
                });
            });

            $(document).on("click", ".menu-delete", function() {
                var delete_submenu_id = $(this).data("id");
                var deleteUrl = "{{ url('admin/submenus') }}/" + delete_submenu_id;
                if (confirm("Are you sure you want to delete this Submenu?")) {
                    $.ajax({
                        type: "DELETE",
                        url: deleteUrl,
                        success: function(response) {
                            if (response.status) {
                                toastr.options = {
                                    closeButton: true,
                                    progressBar: true,
                                };
                                toastr.success(response.message);
                                fetchSubMenu();
                            } else {
                                toastr.options = {
                                    closeButton: true,
                                    progressBar: true,
                                };
                                toastr.error(response.message);
                            }
                        },
                        error: function(error) {
                            toastr.error(error);
                        },
                    });
                }
            });
        });
    </script>
@endpush
