@extends('layouts.master')
@section('css')

@stop
@section('content')

    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <div class="offset-3 col-md-6">
                <div class="card mb-4">
                    <h5 class="card-header">Setting Logo</h5>
                    <div class="card-body">
                        <form action="{{ route('admin.add-logo') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-3">
                                <label for="title" class="form-label">Site Title</label>
                                <input type="text" name="title" class="form-control" id="title"
                                    placeholder="Site Title.." />
                            </div>
                            <div class="mb-3">
                                <label for="name" class="form-label">Logo Name</label>
                                <input type="text" name="name" class="form-control" id="name"
                                    placeholder="Logo Name.." />
                            </div>
                            <div class="mb-3">
                                <label for="formFileMultiple" class="form-label">Logo Image</label>
                                <input class="form-control" name="file" type="file" id="images" />
                            </div>
                            <div class="mb-3">
                                <label for="formFileMultiple" class="form-label">Header</label>
                                <input class="form-control" name="header" type="color" id="header" />
                            </div>
                            <div class="mb-3">
                                <label for="formFileMultiple" class="form-label">Footer</label>
                                <input class="form-control" name="footer" type="color" id="footer" />
                            </div>
                            <input type="hidden" name="id" id="id" value="">
                            <input type="hidden" name="imagesNew" id="imagesNew" value="">
                           
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="offset-2 col-md-8 offset-2">
            <div class="card">
                <h5 class="card-header">Setting Logo List</h5>
                <div>
                    <div class="table-responsive text-nowrap">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Name</th>
                                    <th>Logo</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody class="table-border-bottom-0">
                                @foreach ($lists as $list)
                                    <tr>
                                        <td>
                                            <i class="fab fa-angular fa-lg text-danger me-3"></i>
                                            <span class="fw-medium">{{ $list->title }}</span>
                                        </td>
                                        <td>{{ $list->logo_name }}</td>
                                        <td>
                                            <ul class="list-unstyled users-list m-0 avatar-group d-flex align-items-center">
                                                <li data-bs-toggle="tooltip" data-popup="tooltip-custom"
                                                    data-bs-placement="top" class="avatar avatar-xs pull-up"
                                                    title="Lilian Fuller">
                                                    <img src="{{ url('uploads/logos/' . $list->logo_image) }}"
                                                        alt="Avatar" class="rounded-circle" />
                                                </li>
                                            </ul>
                                        </td>
                                        <td>
                                            @if ($list->status == 1)
                                                <span class="badge bg-label-primary me-1"
                                                    onclick="updateStatus({{ $list->id }});">Active</span>
                                            @else
                                                <span class="badge bg-label-danger me-1"
                                                    onclick="updateStatus({{ $list->id }});">Inactive</span>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn p-0 dropdown-toggle hide-arrow"
                                                    data-bs-toggle="dropdown">
                                                    <i class="bx bx-dots-vertical-rounded"></i>
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="javascript:void(0);" onclick="editRecord({{$list->id}})"><i
                                                            class="bx bx-edit-alt me-1"></i> Edit</a>
                                                    <a class="dropdown-item" href="javascript:void(0);"><i
                                                            class="bx bx-trash me-1"></i> Delete</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @stop
    @section('javascript')
        <script type="text/javascript">
            function updateStatus(id) {
                $.ajax({
                        method: "get",
                        url: "{{ route('admin.setting-status') }}",
                        data: {
                            'set_id': id,
                        }
                    })
                    .done(function(msg) {
                        alert("Status updated!");
                        location.reload();
                    });
            }
            function editRecord(id) {
                
                $.ajax({
                        method: "get",
                        url: "{{ route('admin.setting-edit') }}",
                        data: {
                            'set_id': id,
                        },
                        success:function(result){
                           
                            $('#title').val(result.title);
                            $('#name').val(result.logo_name);
                            $('#imagesNew').val(result.logo_image);
                            $('#header').val(result.logo_header);
                            $('#footer').val(result.logo_footer);
                            $('#id').val(result.id);
                        }
                    })
            }
        </script>
    @stop
