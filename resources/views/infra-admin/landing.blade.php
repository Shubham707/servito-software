@extends('layouts.master')
@section('css')
    <style>
        #editor {
            min-height: 300px;
        }
    </style>
@stop
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">

        <div class="row">

            <div class="card">
                <h5 class="card-header">List Landing Page <button type="button" class="btn btn-outline-primary float-right"
                        data-bs-toggle="modal" data-bs-target="#backDropModal">Add Pages</button></h5>

                <div class="card-body">

                    <div class="mb-3 col-12 mb-0">
                        <div class="table-responsive text-nowrap">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Heading</th>
                                        <th>Title</th>
                                        <th>Image</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="table-border-bottom-0">
                                    @foreach ($landings as $landing)
                                        <tr class="table-default">
                                            <td>
                                                <i class="fab fa-sketch fa-lg text-warning me-3"></i>
                                                <span class="fw-medium">{{ $landing->heading }}</span>
                                            </td>
                                            <td>{{ $landing->title }}</td>
                                            <td>
                                                <ul
                                                    class="list-unstyled users-list m-0 avatar-group d-flex align-items-center">
                                                    <li data-bs-toggle="tooltip" data-popup="tooltip-custom"
                                                        data-bs-placement="top" class="avatar avatar-xs pull-up"
                                                        title="{{ $landing->menu_name }}">
                                                        <img src="{{ asset('uploads/' . strtolower($landing->menu_name) . '/' . $landing->image) }}"
                                                            alt="Avatar" class="rounded-circle" />
                                                    </li>

                                                </ul>
                                            </td>
                                            <td><span class="badge bg-label-primary me-1">Active</span></td>
                                            <td>
                                                <div class="dropdown">
                                                    <button type="button" class="btn p-0 dropdown-toggle hide-arrow"
                                                        data-bs-toggle="dropdown">
                                                        <i class="bx bx-dots-vertical-rounded"></i>
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="javascript:void(0);"><i
                                                                class="bx bx-edit-alt me-1"></i> Edit</a>
                                                        <a class="dropdown-item" href="javascript:void(0);"><i
                                                                class="bx bx-trash me-1"></i> Delete</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>

    <div class="modal fade" id="backDropModal" data-bs-backdrop="static" tabindex="-1">
        <div class="modal-dialog modal-xl" role="document">

            <form class="modal-content" action="{{ route('landing-add') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-md-12">
                    <!-- <ul class="nav nav-pills flex-column flex-md-row mb-3">
                            <li class="nav-item">
                                <a class="nav-link active" href="javascript:void(0);"><i class="bx bx-user me-1"></i>
                                    Account</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="pages-account-settings-notifications.html"><i
                                        class="bx bx-bell me-1"></i> Notifications</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="pages-account-settings-connections.html"><i
                                        class="bx bx-link-alt me-1"></i> Connections</a>
                            </li>
                        </ul> -->
                    <div class="card mb-4">
                        <div class="modal-header">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <!-- Account -->
                        <div class="card-body">

                            <div class="d-flex align-items-start align-items-sm-center gap-4">
                                <img src="{{ asset('admin/assets/img/avatars/1.png') }}" alt="user-avatar"
                                    class="d-block rounded" height="100" width="100" id="uploadedAvatar" />
                                <div class="button-wrapper">
                                    <label for="upload" class="btn btn-primary me-2 mb-4" tabindex="0">
                                        <span class="d-none d-sm-block">Upload new photo</span>
                                        <i class="bx bx-upload d-block d-sm-none"></i>
                                        <input type="file" id="upload" name="file" class="account-file-input"
                                            hidden accept="image/png, image/jpeg" />
                                    </label>
                                    <!-- <button type="button" class="btn btn-outline-secondary account-image-reset mb-4">
                                            <i class="bx bx-reset d-block d-sm-none"></i>
                                            <span class="d-none d-sm-block">Reset</span>
                                        </button> -->

                                    <p class="text-muted mb-0">Allowed JPG, GIF or PNG. Max size of 800K</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="mb-3 col-md-6">
                                    <label for="email" class="form-label">Menu</label>
                                    <!-- <input class="form-control" type="text" id="menu" name="menu"
                                                 placeholder="Menu" /> -->
                                    <select id="menu" name="menu" class="select2 form-select"
                                        onchange="createMenu(this)">
                                        <option value="">Select Menu</option>
                                        @foreach ($menus as $menu)
                                            <option value="{{ $menu->id }}">{{ $menu->menu_name }}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="createmenu" id="createmenu" value="">
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label for="organization" class="form-label">Submenu</label>
                                    <!-- <input type="text" class="form-control" id="submenu" name="submenu"
                                                 /> -->
                                    <select id="submenu" name="submenu" class="select2 form-select">
                                        <option value="">Select Submenu</option>
                                        @foreach ($submenus as $submenu)
                                            <option value="{{ $submenu->id }}">{{ $submenu->submenu_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label for="firstName" class="form-label">Title</label>
                                    <input class="form-control" type="text" id="title" name="title"
                                        autofocus />
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label for="lastName" class="form-label">Heading</label>
                                    <input class="form-control" type="text" name="heading" id="heading" />
                                </div>

                                <div class="mb-3 col-md-12">
                                    <label class="form-label" for="phoneNumber">Decription</label>
                                    <textarea name="description" class="form-control" id="editor" cols="30" rows="10"></textarea>
                                </div>
                                <div class="mb-3 col-md-4">
                                    <label for="address" class="form-label">Icon Image</label>
                                    <input class="form-control" type="text" name="iconimage" id="iconimage" />
                                </div>
                                <div class="mb-3 col-md-4">
                                    <label for="state" class="form-label">Column No</label>
                                    <select id="columnName" name="columnName" class="select2 form-select">
                                        <option value="">Select Column</option>
                                        <option value="1">1</option>
                                    </select>
                                </div>
                                <div class="mb-3 col-md-4">
                                    <label for="zipCode" class="form-label">Status</label>
                                    <select id="status" name="status" class="select2 form-select">
                                        <option>Select Status</option>
                                        <option>table-info</option>
                                        <option>table-warning</option>
                                        <option>table-danger</option>
                                        <option>table-success</option>
                                    </select>
                                </div>
                            </div>
                            <div class="mt-2">
                                <button type="submit" class="btn btn-primary me-2">Save changes</button>
                                <button type="reset" class="btn btn-outline-secondary">Cancel</button>
                            </div>
            </form>
        </div>
        <!-- /Account -->
    </div>
    </form>
    </div>
    </div>

@stop
@section('javascript')
    <!-- <script src="{{ asset('admin/assets/js/pages-account-settings-account.js') }}"></script> -->
    <script type="text/javascript">
        function createMenu(sel) {

            let create = sel.options[sel.selectedIndex].text;
            $('#createmenu').val(create);
        }

        function updateStatus(id) {
            $.ajax({
                    method: "get",
                    url: "{{ route('admin.setting-status') }}",
                    data: {
                        'set_id': id,
                    }
                })
                .done(function(msg) {
                    alert("Status updated!");
                    location.reload();
                });
        }
    </script>
    <script src="https://cdn.ckeditor.com/ckeditor5/12.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
    </script>
@stop
