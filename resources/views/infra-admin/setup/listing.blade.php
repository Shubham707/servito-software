@extends('layouts.master')
@section('css')

@stop
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="py-3 mb-4"><span class="text-muted fw-light">Account Settings /</span> Account</h4>

        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-pills flex-column flex-md-row mb-3">
                    <li class="nav-item">
                        <a class="nav-link active" href="javascript:void(0);"><i class="bx bx-user me-1"></i> Account</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('theme-settings') }}"><i class="bx bx-bell me-1"></i> Theme</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('theme-connections') }}"><i class="bx bx-link-alt me-1"></i>
                            Connections</a>
                    </li>
                </ul>
                <div class="card mb-4">
                    <h5 class="card-header">Theme Add</h5>
                    <!-- Account -->

                    <hr class="my-0" />
                    <div class="card-body">
                        <form action="{{ route('themes.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="mb-3 col-md-6">
                                    <label for="firstName" class="form-label">Upload Theme Image *</label>
                                    <input class="form-control" name="file" type="file" id="images" />
                                </div>
                                <div class="mb-3 col-md-6">
                                    <?php
                
                                    $path="/resources/views/fronts";
                                    $doc_root = $_SERVER['DOCUMENT_ROOT'];
                                    $pathroot= dirname($doc_root, 1);
                                    $dirPath=$pathroot.''.$path;
                                    $files = scandir($dirPath);
                                    ?> 
                                    <label for="lastName" class="form-label">Theme Name *</label>
                                    <select name="theme_name" id="theme_name" class="select2 form-select">
                                        <?php
                                        foreach ($files as $file) {
                                            $filePath = $dirPath . '/' .$file;
                                            if (is_file($filePath)) {
                                        ?>
                                        <option>{{$file}}</option>
                                        <?php } }?>
                                    </select>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label for="currency" class="form-label">Status *</label>
                                    <select name="theme_status" id="theme_status" class="select2 form-select">
                                        <option value="">Select Status</option>
                                        <option value="Active">Active</option>
                                        <option value="Inactive">Inactive</option>
                                    </select>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label for="state" class="form-label">Content</label>
                                    <textarea class="form-control" id="theme_content" name="theme_content"></textarea>
                                </div>
                                <input type="hidden" name="user_id" value="{{ Auth::id() }}">

                            </div>
                            <div class="mt-2">
                                <button type="submit" class="btn btn-primary me-2">Save changes</button>
                                <button type="reset" class="btn btn-outline-secondary">Cancel</button>
                            </div>
                        </form>
                    </div>
                    <!-- /Account -->
                </div>
                <div class="card">
                    <h5 class="card-header">Theme Account</h5>
                    <div class="card-body">
                        <div class="mb-3 col-12 mb-0">
                            <div class="alert alert-success">
                                <h6 class="alert-heading mb-1">Are you sure you want to delete your account?</h6>
                                <p class="mb-0">Once you delete your account, there is no going back. Please be certain.
                                </p>
                            </div>
                            <div class="table-responsive text-nowrap">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Theme Title</th>
                                            <th>Theme Content</th>
                                            <th>Theme Images</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-border-bottom-0">
                                        @foreach ($themes as $list)
                                            <tr>
                                                <td>
                                                    <i class="fab fa-angular fa-lg text-danger me-3"></i>
                                                    <span class="fw-medium">{{ $list->theme_title }}</span>
                                                </td>
                                                <td>{{ $list->theme_content }}</td>
                                                <td>
                                                    <ul
                                                        class="list-unstyled users-list m-0 avatar-group d-flex align-items-center">
                                                        <li data-bs-toggle="tooltip" data-popup="tooltip-custom"
                                                            data-bs-placement="top" class="avatar avatar-xs pull-up"
                                                            title="Lilian Fuller">
                                                            <img src="{{ url('uploads/themes/' . $list->theme_image) }}"
                                                                alt="Avatar" class="rounded-circle" />
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    @if ($list->theme_status == 'Active')
                                                        <span class="badge bg-label-primary me-1"
                                                            onclick="updateStatus({{ $list->id }});">Active</span>
                                                    @else
                                                        <span class="badge bg-label-danger me-1"
                                                            onclick="updateStatus({{ $list->id }});">Inactive</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <div class="dropdown">
                                                        <button type="button" class="btn p-0 dropdown-toggle hide-arrow"
                                                            data-bs-toggle="dropdown">
                                                            <i class="bx bx-dots-vertical-rounded"></i>
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item" href="javascript:void(0);"><i
                                                                    class="bx bx-edit-alt me-1"></i> Edit</a>
                                                            <a class="dropdown-item" href="javascript:void(0);"><i
                                                                    class="bx bx-trash me-1"></i> Delete</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $('.alert-success').hide();
    </script>
@stop
