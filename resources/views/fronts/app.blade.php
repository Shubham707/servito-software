<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <?php
    use App\Models\System;
    $logos = System::where('status', '1')->first();
    ?>
    <title>{{$logos->title}}</title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    <!-- Favicons -->
    <link href="fronts/img/favicon.png" rel="icon">
    <link href="fronts/img/apple-touch-icon.png" rel="apple-touch-icon">
    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">
    <!-- Vendor CSS Files -->
    <link href="{{ asset('fronts/vendor/aos/aos.css') }}" rel="stylesheet">
    <link href="{{ asset('fronts/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('fronts/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('fronts/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('fronts/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('fronts/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
    <link href="{{ asset('fronts/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">
    <!-- Template Main CSS File -->
    <link href="{{ asset('fronts/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('fronts/css/custom-style.css') }}" rel="stylesheet">
    <style>
    #footer {
    font-size: 14px;
    background: {{$logos->logo_footer}} !important;;
    }
    </style>

</head>

<body>
    @include('fronts.header')
    @yield('content')
    @include('fronts.footer')

    <div id="preloader"></div>
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>
    <!-- Vendor JS Files -->
    <script src="{{ asset('fronts/vendor/aos/aos.js') }}"></script>
    <script src="{{ asset('fronts/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('fronts/vendor/glightbox/js/glightbox.min.js') }}"></script>
    <script src="{{ asset('fronts/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('fronts/vendor/php-email-form/validate.js') }}"></script>
    <script src="{{ asset('fronts/vendor/swiper/swiper-bundle.min.js') }}"></script>
    <script src="{{ asset('fronts/vendor/waypoints/noframework.waypoints.js') }}"></script>
    <!-- Template Main JS File -->
    <script src="{{ asset('fronts/js/main.js') }}"></script>
    <script type="text/javascript">
        function onUrls() {
            window.location.href = "{{ url('/') }}";
        }
    </script>
</body>

</html>
