<?php
    use App\Models\System;
    $logos = System::where('status', '1')->first();
    ?>
<header id="header" class="fixed-top" style="background:{{$logos->logo_header}} !important;">
    <div class="container d-flex align-items-center">
    
        <!-- <h1 class="logo me-auto">
            <img src="">
            <a href="{{ url('/') }}">Servito Software</a>
        </h1> -->
        <a href="{{ url('/') }}" class="logo me-auto">
            <img src="{{url('uploads/logos/'.$logos->logo_image)}}" alt="" class="img-fluid">
        </a>
        <nav id="navbar" class="navbar">
            <ul>
                <?php
                use App\Models\Submenu;
                use App\Models\Menu;
                $menus = Menu::all();
                $lists = Submenu::join('menus', 'submenus.menu_id', '=', 'menus.id')->get();
                ?>
                @foreach ($menus as $menu)
                    <?php $submenu = Submenu::where('menu_id', $menu->id)->first(); ?>
                    @if ($menu->menu_name == 'Home')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/') }}">{{ $menu->menu_name }}</a>
                        </li>
                    @elseif ($submenu)
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="{{ $menu->menu_name }}"
                                href="{{ url('pages/' . strtolower($menu->menu_name)) }}" data-bs-toggle="dropdown"
                                data-bs-auto-close="outside">{{ $menu->menu_name }}</a>
                            <div class="dropdown-menu shadow">
                                <div class="container">
                                    <div class="row d-flex">
                                        @foreach ($lists as $list)
                                            @if ($list->menu_id == $menu->id)
                                                <div class="col-4">
                                                    <div class="p-0"> <!-- Add p-0 class to remove padding -->
                                                        <a class="text-dark"
                                                            href="{{ url(strtolower('pages/' . $menu->menu_name . '/' . $list->submenu_name)) }}">{{ $list->submenu_name }}</a>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" id="" href="#{{ $menu->menu_name }}">
                                {{ $menu->menu_name }}
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav>


    </div>
</header>
