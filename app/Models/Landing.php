<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Landing extends Model
{
    use HasFactory;
    protected $table="landings";
    protected $fillable=['user_id','menu_id','submenu_id','heading','image','title','decription','iconImage','column_no','status'];
}
