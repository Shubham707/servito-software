<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class System extends Model
{
    use HasFactory;
    protected $table = "systems";
    //ALTER TABLE `systems` ADD `logo_header` VARCHAR(50) NOT NULL AFTER `logo_name`, ADD `logo_footer` VARCHAR(50) NOT NULL AFTER `logo_header`;
    protected $fillable = [
        'name',
        'email',
        'logo_header',
        'logo_footer',
        'password',
        'status'
    ];
}
