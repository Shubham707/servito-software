<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
    use HasFactory;
    protected $table="themes";
    protected $fillable=['theme_title','theme_image','theme_status','user_id','theme_content'];
}
