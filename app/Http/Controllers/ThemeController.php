<?php

namespace App\Http\Controllers;

use App\Models\Theme;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ThemeController extends Controller
{
    
    
    public function index()
    {
        $themes=Theme::all();
        return view('infra-admin.setup.listing',compact('themes'));
    }
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        if($files=$request->file('file')){  
            $name=time().'_'.$files->getClientOriginalName();  
            $files->move('uploads/themes',$name);    
        }
        $project = new Theme();
        $project->theme_title = $request->theme_name;
        $project->theme_content = $request->theme_content;
        $project->theme_status = $request->theme_status;
        $project->user_id = $request->user_id;
        $project->theme_image = $name;
        $project->save();
        return back();
    }

    
    public function show(Theme $theme)
    {
        //
    }

    
    public function edit(Theme $theme)
    {
        //
    }

    public function update(Request $request, Theme $theme)
    {
        
    }

    public function destroy(Theme $theme)
    {
        //
    }
    public function connections(){
        return view('infra-admin.setup.connections');
    }
    public function settings(){
        return view('infra-admin.setup.settings');
    }
    
}
