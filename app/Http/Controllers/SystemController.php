<?php

namespace App\Http\Controllers;

use App\Models\System;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
class SystemController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $lists = System::all();   
        return view('infra-admin.setting',compact('lists'));
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {  

        if($request->hasFile('file')){ 
           
            $files=$request->file('file');
            $name=time().'_'.$files->getClientOriginalName();  
            $files->move('uploads/logos',$name);  
            if($request->imagesNew !=""){
                $filedeleted = unlink('uploads/logos/'.$request->imagesNew);  
            }
            
        }else{
            $name=$request->imagesNew;
        }

        if($request->id !=""){
            $project = System::find($request->id);
        }else{
            $project = new System();
        }
        
        $project->title = $request->title;
        $project->logo_name = $request->name;
        $project->logo_image = $name;
        $project->logo_header = $request->header;
        $project->logo_footer =  $request->footer;
        $project->save();
        return back();
    }

   
    public function show(System $system)
    {
        //
    }

    public function edit(Request $request)
    {
        $id=$request->set_id;
       return  $user=System::findOrFail($id);
    }

    public function update(Request $request, System $system)
    {
        //
    }

    
    public function destroy(System $system)
    {
        //
    }
    public function statusChange(Request $request){
        $id=$request->set_id;
        $user=System::findOrFail($id);
        if($user->status==1){
            $user->status=0;
        }else{
            $user->status=1;
        }
       return $user->update();
    }
}
