<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Landing;
use App\Models\Submenu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class PagesController extends Controller
{
    public function pages(Request $req)
    {
        if ($req->first != "" && $req->second != "") {
            $menu = $req->first;
            $submenu = $req->second;
            $landings = Landing::join('menus', 'menus.id', '=', 'landings.menu_id')
                ->join('submenus', 'submenus.id', '=', 'landings.submenu_id')
                ->where('menus.menu_name', $menu)
                ->where('submenus.submenu_name', $submenu)
                ->get();
            // /dd($landings);
           
        } else {
            
            $landings = Landing::join('menus', 'menus.id', '=', 'landings.menu_id')
                ->join('submenus', 'submenus.id', '=', 'landings.submenu_id')
                ->get();
            // /dd($landings);
            /////
           
        }
        return view('welcome', compact('landings'));
        // $menu = $req->first;
        // $submenu = $req->second;
        // $landings = Landing::join('menus', 'menus.id', '=', 'landings.menu_id')
        //     ->join('submenus', 'submenus.id', '=', 'landings.submenu_id')
        //     ->where('menus.menu_name', $menu)
        //     ->where('submenus.submenu_name', $submenu)
        //     ->get();
        // // /dd($landings);
        // return view('welcome', compact('landings'));
        // return Redirect::to('/')->with('landings', $landings);
    }
}
