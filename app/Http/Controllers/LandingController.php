<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Landing;
use App\Models\Submenu;
use App\Models\Menu;
use Illuminate\Support\Facades\Auth;
class LandingController extends Controller
{
    public function index(){
        $landings=Landing::join('menus', 'menus.id', '=', 'landings.menu_id')->get();
        $submenus=Submenu::all();
        $menus=Menu::all();
        return view('infra-admin.landing',compact('landings','submenus','menus'));
    }
    public function store(Request $request){
        
        $location = "uploads/".strtolower($request->createmenu);

        if(!is_dir($location)){
           mkdir($location, 0755);
        }
        if($files=$request->file('file')){  
            $name=time().'_'.$files->getClientOriginalName();  
            $files->move($location,$name);    
        }
        $project = new Landing();
        $project->title = $request->menu;
        $project->menu_id = $request->submenu;
        $project->submenu_id = $request->submenu;
        $project->heading = $request->heading;
        $project->title = $request->title;
        $project->decription = $request->description;
        $project->iconImage = $request->iconimage;
        $project->column_no = $request->columnName;
        $project->status = $request->status;
        $project->image = $name;
        $project->user_id = Auth::id();
        $project->save();
        return back();
    }
    
}
