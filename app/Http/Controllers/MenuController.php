<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMenuRequest;
use App\Http\Requests\UpdateMenuRequest;
use Illuminate\Support\Facades\Validator;

class MenuController extends Controller
{
    public function index()
    {
        return view('infra-admin.menu.menu');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        if ($request->ajax()) {
            $rules = [
                'menu_name' => 'required|string|unique:menus,menu_name',
            ];
            $messages = [
                'menu_name.required' => 'Menu name is required',
                'menu_name.unique' => 'Menu name already exists',
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                return response()->json([
                    'status' => false,
                    'errors' => $validator->errors(),
                ]);
            }
            $menu = new Menu();
            $menu->menu_name = $request->input('menu_name');
            $menu->save();
            return response()->json([
                'status' => true,
                'message' => 'Menu Added Successfully',
            ]);
        }
    }

    public function fetchMenu(Request $request)
    {
        try {
            $menus = Menu::all();
            if ($menus->isEmpty()) {
                return response()->json(['message' => 'No menus found'], 404);
            }
            return response()->json($menus);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Error fetching menus'], 500);
        }
    }

    public function show(Menu $menu)
    {
        // 
    }

    public function edit($id)
    {
        $menu = Menu::find($id);
        if (empty($menu)) {
            return response()->json([
                'status' => false,
                'message' => 'Menu not found'
            ]);
        }
        return response()->json([
            'status' => true,
            'menu' => $menu
        ]);
    }

    public function update(Request $request, $id)
    {
        if ($request->ajax()) {
            $rules = [
                'edit_menu_name' => 'required|string|unique:menus,menu_name',
            ];
            $messages = [
                'edit_menu_name.required' => 'Menu name is required',
                'edit_menu_name.unique' => 'Menu name already exists',
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                return response()->json([
                    'status' => false,
                    'errors' => $validator->errors(),
                ]);
            }

            $menu = Menu::find($id);
            if (!$menu) {
                return response()->json([
                    'status' => false,
                    'message' => 'Menu not found',
                ]);
            }

            $menu->menu_name = $request->input('edit_menu_name');
            $menu->save();

            return response()->json([
                'status' => true,
                'message' => 'Menu Updated Successfully',
            ]);
        }
    }

    public function destroy($id)
    {
        $menu = Menu::find($id);
        if (!$menu) {
            return response()->json([
                'status' => false,
                'message' => 'Menu not found',
            ]);
        }
        $menu->delete();

        return response()->json([
            'status' => true,
            'message' => 'Menu Deleted Successfully',
        ]);
    }
}
