<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Submenu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreSubmenuRequest;
use App\Http\Requests\UpdateSubmenuRequest;

class SubmenuController extends Controller
{
    public function index()
    {
        $menus = Menu::all();
        return view('infra-admin.menu.submenu', compact('menus'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        if ($request->ajax()) {
            $rules = [
                'submenu' => 'required|string|unique:menus,menu_name',
                'menu' => 'required',
            ];
            $messages = [
                'menu.required' => 'Please Select Menu',
                'submenu.required' => 'Sumenu name is required',
                'submenu.unique' => 'Sumenu name already exists',
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                return response()->json([
                    'status' => false,
                    'errors' => $validator->errors(),
                ]);
            }
            $submenu = new Submenu();
            $submenu->menu_id = $request->menu;
            $submenu->submenu_name = $request->submenu;
            $submenu->save();
            return response()->json([
                'status' => true,
                'message' => 'Submenu Added Successfully',
            ]);
        }
    }

    public function fetchSubMenu(Request $request)
    {
        try {
            $submenu = Submenu::join('menus', 'submenus.menu_id', '=', 'menus.id')->get();
            if ($submenu->isEmpty()) {
                return response()->json(['message' => 'Submenus Not Found'], 404);
            }
            return response()->json([
                'status' => true,
                'submenu' => $submenu,
            ]);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Error Fetching Submenus'], 500);
        }
    }

    public function show(Submenu $submenu)
    {
        //
    }

    public function edit($id)
    {
        $submenu = Submenu::join('menus', 'submenus.menu_id', '=', 'menus.id')->where('submenus.menu_id', $id)->first();
        if (empty($submenu)) {
            return response()->json([
                'status' => false,
                'message' => 'Submenu Not Found'
            ]);
        }
        return response()->json([
            'status' => true,
            'submenu' => $submenu
        ]);
    }
    public function update(Request $request, $id)
    {
        if ($request->ajax()) {
            $rules = [
                'submenu' => 'required|string|unique:menus,menu_name',
                'menu' => 'required',
            ];
            $messages = [
                'menu.required' => 'Please Select Menu',
                'submenu.required' => 'Sumenu name is required',
                'submenu.unique' => 'Sumenu name already exists',
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                return response()->json([
                    'status' => false,
                    'errors' => $validator->errors(),
                ]);
            }
            $submenu = Submenu::find($id);
            if (!$submenu) {
                return response()->json([
                    'status' => false,
                    'message' => 'Submenu Not Found',
                ]);
            }

            $submenu->menu_id = $request->menu;
            $submenu->submenu_name = $request->submenu;
            $submenu->save();
            return response()->json([
                'status' => true,
                'message' => 'Submenu Added Successfully',
            ]);
        }
    }

    public function destroy($id)
    {
        $Submenu = Submenu::find($id);
        if (!$Submenu) {
            return response()->json([
                'status' => false,
                'message' => 'Submenu not found',
            ]);
        }
        $Submenu->delete();
        return response()->json([
            'status' => true,
            'message' => 'Submenu Deleted Successfully',
        ]);
    }
}
